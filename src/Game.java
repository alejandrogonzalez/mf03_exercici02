import java.util.Random;
import java.util.Scanner;

public class Game {
    public static void main(String[] args) {

    }

    public void inici(){
        while (true){
            try{
                System.out.println("Decide una de las tres opciones: Piedra, papel o tijera");
                String jugador = decisionJugador();
                String enemigo = decisionEnemigo();

            }catch(TrampaException e){

            }
        }

    }

    public String decisionJugador() throws TrampaException{
        Scanner lector = new Scanner(System.in);
        String piedra = "piedra";
        String papel = "papel";
        String tijera = "tijera";
        String respuestaJugador = lector.nextLine();

        if(respuestaJugador.equalsIgnoreCase("piedra")) {
            respuestaJugador = piedra;
        }else if(respuestaJugador.equalsIgnoreCase("papel")){
            respuestaJugador = papel;

        }else if(respuestaJugador.equalsIgnoreCase("tijera")){
            respuestaJugador = tijera;
        }else{
            throw new TrampaException();
        }

        return respuestaJugador;
    }



    public String decisionEnemigo(){
        Random random = new Random();
        int respuestaEnemigo = 1 + random.nextInt(3);

        String decision = null;

        switch (respuestaEnemigo){
            case 1:
                decision ="piedra";
                break;
            case 2:
                decision = "papel";
                break;
            case 3:
                decision = "tijera";
                break;
        }
        return decision;
    }

    public void resultado(){

    }


}




class TrampaException extends Exception{
    public TrampaException(){
        super("Has hecho trampas, vuelve a introducir uno de las tres opciones");
    }
    public TrampaException(String s){
        super(s);
    }
}
